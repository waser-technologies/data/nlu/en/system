.PHONY: install_requirements, action, validate, train, shell, serve

install_requirements:
	@pip install -r requirements.txt

action:
	@rasa run actions --debug
	@killall rasa

validate:
	@rasa data validate --data ./data/ -d ./domain.yml -c ./config.yml

train:
	@rasa train --data ./data/ -d ./domain.yml -c ./config.yml --out ./models

shell:
	@rasa shell -m models/ --enable-api --endpoints ./endpoints.yml --credentials ./credentials.yml

serve:
	@rasa run -m models/ --enable-api --endpoints ./endpoints.yml --credentials ./credentials.yml
	@killall rasa