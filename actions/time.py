import random
import locale

from datetime import time
from dateutil.relativedelta import relativedelta

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional
from num2words import num2words

from .date import get_current_time as now

locale.setlocale(locale.LC_TIME,'')

def an_hour():
    an_hour = relativedelta(hours=1)
    return an_hour

def is_morning(n = now()) -> bool:
    if n >= time(00,00) and n <= time(11,59):
        return True
    return False

def is_afternoon(n = now()) -> bool:
    if n >= time(12,00) and n <= time(17,59):
        return True
    return False

def is_evening(n = now()) -> bool:
    if n >= time(18,00) and n <= time(23,59):
        return True
    return False

class ActionGiveTime(Action):

    def name(self) -> Text:
        return "action_be_what_it_time"

    def spell_number(self, n: float) -> Text:
        return num2words(n, lang='en')

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        
        half_hour_flag = False

        date = now()
        _h = int(date.hour)
        _m = int(date.minute)

        if _m < 40:
            # Heure spéciales
            # h = 0
            if _h == 0:
                if _m == 0:
                    # Il est minuit.
                    dispatcher.utter_message(text="It's midnight.")
                    return []
                h = "midnight"
            # h = 12
            elif _h == 12:
                if _m == 0:
                    dispatcher.utter_message(text="It's noon.")
                    return []
                h = "noon"
            # h > 12
            elif _h > 12:
                # TODO: ask the system for date format. 12h or 14h
                bais = [0, 0, 12, 0, 12]
                b = random.choice(bais)
                if b == 12:
                    half_hour_flag = True
                h = self.spell_number(_h - b)
            else:
                h = self.spell_number(_h)

            # minutes spéciales
            # 00
            if _m == 0:
                # il est _h heure pile.
                dispatcher.utter_message(response="utter_be_what_it_hours", hours=h)
                return []
            # 15
            if _m == 15:
                # il est _h-12 heures et quart.
                if _h > 12 and half_hour_flag == True: # you don't say 18 heures et quart... but 6 heures et quart
                    m = "quarter"
                    dispatcher.utter_message(response="utter_be_what_it_hours_minutes", hours=h, minutes=m)
                    return []
                else:
                    m = self.spell_number(_m)
                    dispatcher.utter_message(response="utter_be_what_it_hours_minutes", hours=h, minutes=m)
                    return []
            # 30
            elif _m == 30:
                # il est _h-12 heures et de-mi.
                if _h > 12 and half_hour_flag == True: # you don't say 18 heures et de-mi... but 6 heures et de-mi
                    m = "half"
                    dispatcher.utter_message(response="utter_be_what_it_hours_minutes", hours=h, minutes=m)
                    return []
                else:
                    m = self.spell_number(_m)
                    dispatcher.utter_message(response="utter_be_what_it_hours_minutes", hours=h, minutes=m)
                    return []
            else:
                m = self.spell_number(_m)
                dispatcher.utter_message(response="utter_be_what_it_hours_minutes", hours=h, minutes=m)
                return []
        else:
            # Il est 10 heures -20
            if _h < 12:
                _h += 1
                _m = 60 - _m
            elif _h > 12:
                b = 12
                _h = _h - b + 1
                _m = 60 - _m
            
            if _h == 0:
                h = "midnight"
                m = self.spell_number(_m)

                dispatcher.utter_message(response="utter_be_what_it_time", hours=h, minutes=m)
                return []
            elif _h == 12:
                h = "noon"
                m = self.spell_number(_m)

                dispatcher.utter_message(response="utter_be_what_it_time", hours=h, minutes=m)
                return []
            else:
                h = self.spell_number(_h)

                if _m == 15:
                    m = "quarter"
                else:
                    m = self.spell_number(_m)
                
                dispatcher.utter_message(response="utter_be_what_it_hours_minutes_for", hours=h, minutes=m)
                return []
        return []