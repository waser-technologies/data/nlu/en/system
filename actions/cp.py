import os
import pathlib
import shutil

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .pwd import get_pwd
from .path import (
    get_path_from_query, 
    does_exist, 
    is_dir, 
    is_file, 
    parent_dir, 
    pronounce_path
)

def copy_file(src, dst):
    if is_dir(src):
        if does_exist(dst):
            bsrc = os.path.basename(src)
            dst = os.path.join(dst, bsrc)
        shutil.copytree(src, dst)
    elif is_file(src):
        shutil.copy2(src, dst)
    return dst

class ActionCopyFiles(Action): #cp
    
    def name(self) -> Text:
        return "action_copy_which_files"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        cmd = "cp"
        as_path = tracker.latest_message['text'].split()[1:] if tracker.latest_message['text'].split()[0] == cmd else tracker.get_latest_entity_values("as_path") or get_path_from_query(tracker.latest_message['text']) or [tracker.get_slot("as_path")] or []
        
        if len(as_path) < 2:
            dispatcher.utter_message(response="utter_copy_files_notprovided")
        else:
            target_path = as_path[-1]
            source_files = as_path[:-1]
            
            if does_exist(parent_dir(target_path)):
                dispatcher.utter_message(response="utter_copy_which_files", as_path=pronounce_path(target_path))
                for p in source_files:
                    if does_exist(p):
                        copy_file(p, target_path)
                    else:
                        dispatcher.utter_message(response="utter_copy_source_notexist", as_path=pronounce_path(p))
            else:
                dispatcher.utter_message(response="utter_copy_target_notexist", as_path=pronounce_path(parent_dir(target_path)))

        return []