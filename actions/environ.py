import os

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

def get_env(env_key, or_else=None):
    return os.environ.get(env_key, or_else)

def set_env(env_key, env_val):
    os.environ[env_key] = env_val
    return env_val

def get_env_report():
    report = "\n"
    for k, v in os.environ.items():
        report = report + f"{k}: {v}\n"
    return report

class ActionGetCurrentEnvironment(Action): #printenv
    
    def name(self) -> Text:
        return "action_be_what_environment_currently"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        
        #os.system('/usr/bin/gnome-terminal -- $SHELL -c \'printenv|$EDITOR; exec $SHELL\'')
        env_report = get_env_report()

        dispatcher.utter_message(response="utter_be_what_environment_currently", env=env_report)

        return []
