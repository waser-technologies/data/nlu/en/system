import random

from rasa_sdk import Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType, FollowupAction
from typing import Dict, Text, List, Any, Optional

from .time import is_morning, is_afternoon, is_evening
from .names import get_list_firstnames, get_list_lastnames
from .us import get_us_from_memory, ME, update_us
from .me import get_me_from_memory, MEMORY_PATH

def get_assistant() -> str:
    # No assistant found in memory
    
    # Settings up a default one.
    
    # Save assistant in memory

    return "assistant"

def get_greetings_action_name() -> str:
    # if evening, say good evening
    if is_evening():
        return "utter_have_good_me_evening"
    # if after noon, say good afternoon
    elif is_afternoon():
        return "utter_have_good_me_afternoon"
    # if morning, say good morning
    elif is_morning():
        return "utter_have_good_me_morning"
    # else say good day
    else:
        return "utter_have_good_me_day"

class ActionFirstContact(FormValidationAction):
    # def __init__(self):
    #     super().__init__()

    def name(self) -> Text:
        return "validate_first_contact_form"

    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
        ) -> Optional[List[Text]]:
        first_name = tracker.slots.get("first_name")
        me_firstname = tracker.slots.get("me_firstname")
        if first_name is not None and not me_firstname:
            if first_name.lower() not in get_list_firstnames():
                return ["firstname_spelled_correctly"] + slots_mapped_in_domain
        last_name = tracker.slots.get("last_name")
        me_lastname = tracker.slots.get("me_lastname")
        if last_name is not None and not me_lastname:
            if last_name.lower().capitalize() not in get_list_lastnames():
                return ["lastname_spelled_correctly"] + slots_mapped_in_domain
        return slots_mapped_in_domain

    async def extract_firstname_spelled_correctly(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> Dict[Text, Any]:
        intent = tracker.get_intent_of_latest_message()
        return {"firstname_spelled_correctly": intent == "affirm_it_me"}
    
    async def extract_lastname_spelled_correctly(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> Dict[Text, Any]:
        intent = tracker.get_intent_of_latest_message()
        return {"lastname_spelled_correctly": intent == "affirm_it_me"}

    def validate_firstname_spelled_correctly(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
        ) -> Dict[Text, Any]:
        """Validate `first_name` value."""
        if tracker.get_slot("firstname_spelled_correctly"):
            return {"first_name": tracker.get_slot("first_name"), "firstname_spelled_correctly": True}
        return {"first_name": None, "firstname_spelled_correctly": None}
    
    def validate_lastname_spelled_correctly(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
        ) -> Dict[Text, Any]:
        """Validate `last_name` value."""
        if tracker.get_slot("lastname_spelled_correctly"):
            return {"last_name": tracker.get_slot("first_name"), "lastname_spelled_correctly": True}
        return {"last_name": None, "lastname_spelled_correctly": None}

    def validate_me_firstname(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
        ) -> Dict[Text, Any]:
        """Validate `first_name` value."""

        if len(slot_value) <= 1:
            dispatcher.utter_message(text=f"That's a very short name. I'm assuming you mis-spelled.")
            return {"me_firstname": None}
        else:
            return {"me_firstname": slot_value}

    def validate_me_lastname(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
        ) -> Dict[Text, Any]:
        """Validate `last_name` value."""
        if len(slot_value) <= 1:
            dispatcher.utter_message(text=f"That's a very short name. I'm assuming you mis-spelled.")
            return {"me_lastname": None}
        else:
            return {"me_lastname": slot_value}
    
    def validate_me_gender(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
        ) -> List[Dict[Text, Any]]:
        return {'me_gender': slot_value}
    
    def validate_me_surname(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
        ) -> List[Dict[Text, Any]]:
        if len(slot_value) <= 1:
            dispatcher.utter_message(text=f"That's a very short name. I'm assuming you mis-spelled.")
            return {"me_surname": None}
        else:
            return {'me_surname': slot_value}

class ActionFirstWelcome(Action):
    
    def name(self) -> Text:
        return "action_first_welcome"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> List[EventType]:
        
        _you, _me = get_us_from_memory()
        me_firstname = tracker.get_slot("me_firstname")
        if me_firstname:
            _me['first_name'] = me_firstname.capitalize()
        else:
            me_firstname = _me.get("first_name", "")
        
        me_lastname = tracker.get_slot("me_lastname")
        if me_lastname:
            _me['last_name'] = me_lastname.capitalize()
        else:
            me_lastname = _me.get("last_name", "")
        
        me = tracker.get_slot("me")
        if me:
            _me['surname'] = me.capitalize()
        else:
            me = _me.get("surname", "")
        
        me_gender = tracker.get_slot("me_gender")
        if me_gender:
            _me['gender'] = me_gender
        else:
            me_gender = _me.get("gender", "")
        
        update_us(_you, _me, f"{ASSISTANT_DB_PATH}")

        dispatcher.utter_message(response="utter_glad_meet__me", me=me)
        return []

class ActionIntroduceFirstContact(Action):

    def name(self) -> Text:
        return "action_introduce_first_contact"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> List[EventType]:

        dispatcher.utter_message(response="utter_introduce_me_you")

        return [FollowupAction("first_contact_form")]

class ActionGreet(Action):

    def name(self) -> Text:
        return "action_greet"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> List[EventType]:

        # Initialize the next state
        next_states = []
        nsaf = False # Next States Append Follow-up (Action)

        # Select the appropriate formula to greet
        greeting_action_name = get_greetings_action_name()
        greeting_action_surname = greeting_action_name + "__me"

        me_firstname = tracker.get_slot("me_firstname")
        me_lastname = tracker.get_slot("me_lastname")
        me_names = tracker.get_slot("me_names")
        me_gender = tracker.get_slot("me_gender")
        me = tracker.get_slot("me")
        you = tracker.get_slot("you")

        _you, _me = get_us_from_memory(f"{MEMORY_PATH}")
        
        _me_firstname = _me.get('first_name', None)
        _me_lastname = _me.get('last_name', None)
        _me_gender = _me.get('gender', None)
        _me_names = _me.get('names', [ME.capitalize()])
        _me_surname = _me.get('surname', random.choice(_me_names) if _me_names else ME.capitalize())
        _you_name = _you.get('agent', get_assistant())
        _you_agents = _you.get('agents', [get_assistant()])

        if not _me_firstname and not nsaf:
            next_states.append(FollowupAction("action_introduce_first_contact"))
            nsaf = True
        elif not me_firstname and _me_firstname:
            next_states.append(SlotSet('me_firstname', _me_firstname))

        if not _me_lastname and not nsaf:
            next_states.append(FollowupAction("action_introduce_first_contact"))
            nsaf = True
        elif not me_lastname and _me_lastname:
            next_states.append(SlotSet('me_lastname', _me_lastname))

        if not _me_gender and not nsaf:
            next_states.append(FollowupAction("action_introduce_first_contact"))
            nsaf = True
        elif not me_gender and _me_gender:
            next_states.append(SlotSet('me_gender', _me_gender))

        if not _me_surname and not nsaf:
            next_states.append(FollowupAction("action_introduce_first_contact"))
            nsaf = True
        elif not me and _me_surname:
            next_states.append(SlotSet('me', _me_surname))

        if not _me_names and not nsaf:
            next_states.append(FollowupAction("action_introduce_first_contact"))
            nsaf = True
        elif not me_names and _me_names:
            next_states.append(SlotSet('me_names', _me_names))
        
        if not _you_name and not nsaf:
            next_states.append(FollowupAction("action_introduce_first_contact"))
            nsaf = True
        elif not you and _you_name:
            next_states.append(SlotSet('you', _you_name))

        if me and you:
            dispatcher.utter_message(response=greeting_action_surname, me=me)
        else:
            dispatcher.utter_message(response=greeting_action_name)

        return next_states
