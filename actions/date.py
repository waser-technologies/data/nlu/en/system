import time
import random
import locale

from datetime import datetime, time
from dateutil.relativedelta import relativedelta

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from num2words import num2words

locale.setlocale(locale.LC_TIME,'')

def get_current_datetime():
    "Returns now as datetime.datetime"
    return datetime.now()

def get_current_time():
    "Returns now as datetime.datetime.time"
    return get_current_datetime().time()

def a_year():
    a_year = relativedelta(years=1)
    return a_year

def a_month():
    a_month = relativedelta(months=1)
    return a_month

class ActionGiveDate(Action): #date

    def name(self) -> Text:
        return "action_be_what_it_date"

    def spell_number(self, n: float) -> Text:
        return num2words(n, lang='en')

    def get_ordinal(self, n: float) -> Text:
        return num2words(n, to='ordinal', lang='en')

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        d = int(time.strftime('%d'))
        day_name = time.strftime('%A')
        day_num = self.spell_number(d)

        month_num = time.strftime('%m')
        month_name = time.strftime('%B')
        
        year_num = int(time.strftime('%Y'))
        year_name = self.spell_number(year_num)

        day_num_ordinal = self.get_ordinal(d)
        month_name_ordinal = self.get_ordinal(int(month_num))

        dispatcher.utter_message(response="utter_be_what_it_date", day_week=day_name, day=day_num, month=month_name, year=year_name, day_ordinal=day_num_ordinal, month_ordinal=month_name_ordinal)

        return []