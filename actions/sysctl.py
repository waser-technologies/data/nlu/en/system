import os
import yaml

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional


def sysctl(service, action, exec_path="/usr/bin/systemctl", is_me=True, is_now=False):
    u = "--user" if is_me else ""
    n = "--now" if is_now else ""
    cmd = f"{exec_path} {u} {n} {action} {service}"
    
    return os.system(cmd)

def read_conf(conf_path):
    with open(conf_path, 'r') as c:
        return yaml.load(c)

def edit_conf(conf, conf_path):
    with open(conf_path, 'w') as f:
        yaml.dump(conf, f)

base_conf_path = os.path.expanduser("~/.assistant")
stt_conf_path = f"{base_conf_path}/stt.toml"
tts_conf_path = f"{base_conf_path}/tts.toml"

# You can speak
class ActionAllowToSpeak(Action):
    
    def name(self):
        return "action_allow_speak"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        conf = read_conf(tts_conf_path)

        is_already_allowed_to_speak = conf['tts'].get('is_allowed', False)
        if is_already_allowed_to_speak:
            dispatcher.utter_message(response="utter_can_talk_still_you")
        else:
            conf['tts']['is_allowed'] = True
            edit_conf(conf, tts_conf_path)
            sysctl("speak", "enable", is_now=True)
            dispatcher.utter_message(response="utter_can_talk_you")

        return []

# You can't speak
class ActionDenyToSpeak(Action):
    
    def name(self):
        return "action_deny_speak"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        
        conf = read_conf(tts_conf_path)

        is_already_denied_to_speak = conf['tts'].get('is_allowed', False)
        if not is_already_denied_to_speak:
            dispatcher.utter_message(response="utter_can_not_talk_still_you")
        else:
            conf['tts']['is_allowed'] = False
            edit_conf(conf, tts_conf_path)
            sysctl("speak", "disable", is_now=True)
            dispatcher.utter_message(response="utter_can_not_talk_you")

        return []

# You can listen
class ActionAllowToListen(Action):
    
    def name(self):
        return "action_allow_listen"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        conf = read_conf(stt_conf_path)

        is_already_allowed_to_listen = conf['stt'].get('is_allowed', False)
        if is_already_allowed_to_listen:
            dispatcher.utter_message(response="utter_can_listen_still_you")
        else:
            conf['stt']['is_allowed'] = True
            edit_conf(conf, stt_conf_path)
            sysctl("listen", "enable", is_now=True)
            dispatcher.utter_message(response="utter_can_listen_you")
        
        return []

# You can't listen
class ActionDenyToListen(Action):

    def name(self):
        return "action_deny_listen"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        conf = read_conf(stt_conf_path)

        is_already_denied_to_listen = conf['stt'].get('is_allowed', False)
        if not is_already_denied_to_listen:
            dispatcher.utter_message(response="utter_can_not_listen_still_you")
        else:
            conf['stt']['is_allowed'] = False
            edit_conf(conf, stt_conf_path)
            sysctl("listen", "disable", is_now=True)
            dispatcher.utter_message(response="utter_can_not_listen_you")

        return []
