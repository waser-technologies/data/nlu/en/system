import os
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .environ import get_env, set_env
from .path import pronounce_path

def get_pwd():
    if get_env('PWD') != os.getcwd():
        set_env('PWD', os.getcwd())
    return get_env('PWD')

def set_pwd(dir=None):
    u = get_env('USERNAME', "root")
    h = get_env('HOME', f"/home/{u}" if u != "root" else "/")
    os.chdir(dir)
    return get_pwd()

class ActionGetCurrentDirectory(Action): #PWD
    
    def name(self) -> Text:
        return "action_be_which_dir_currently"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        pwd = get_pwd()
        dispatcher.utter_message(response="utter_be_which_dir_currently", pwd=pronounce_path(pwd))

        return []
