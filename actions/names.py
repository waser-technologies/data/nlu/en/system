import os
from pathlib import Path
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType, FollowupAction
from typing import Dict, Text, List, Any, Optional
from .exception import FileNotExistsOrNotFileError

def get_list_firstnames():
    p = Path("../data/en/nlu/SmallTalk/lookup/firstnames.txt")
    if not p.exists():
        loc_name_list_url = "https://raw.githubusercontent.com/RasaHQ/rasa-2.x-form-examples/main/06-custom-name-experience/data/names.txt"
        os.system(f"wget {loc_name_list_url} -d {p.parent.as_posix()};mv names.txt firstnames.txt")
        names = p.read_text().split("\n")
    elif p.exists() and p.is_file():
        names = p.read_text().split("\n")
    else:
        raise FileNotExistsOrNotFileError(p.exists(), p.is_file())
    return names

def get_list_lastnames():
    p = Path("../data/en/nlu/SmallTalk/lookup/lastnames.txt")
    if not p.exists():
        pass # :(
    elif p.exists() and p.is_file():
        names = p.read_text().split("\n")
    else:
        raise FileNotExistsOrNotFileError(p.exists(), p.is_file())
    return names

def get_list_surnames():
    p = Path("../data/en/nlu/SmallTalk/lookup/surnames.txt")
    if not p.exists():
        pass # :(
    elif p.exists() and p.is_file():
        names = p.read_text().split("\n")
    else:
        raise FileNotExistsOrNotFileError(p.exists(), p.is_file())
    return names

class ActionSetUserFirstName(Action):

    def name(self) -> Text:
        return "action_be_firstname_me"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> List[EventType]:

        me_firstname = tracker.latest_message['entities'][0].get('first_name')

        if me_firstname:
            if me_firstname in get_list_firstnames():
                dispatcher.utter_message(text=f"Very well {me_firstname}.")
                return [SlotSet("me_firstname", me_firstname)]
        
        dispatcher.utter_message(text=f"Very funny {me_firstname}.")
        
        return []

class ActionSetUserLastName(Action):

    def name(self) -> Text:
        return "action_be_lastname_me"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> List[EventType]:

        me_lastname = tracker.latest_message['entities'][0].get('last_name')
        me_gender = tracker.get_slot("me_gender")

        if me_lastname:
            if me_lastname in get_list_lastnames():
                if me_gender:
                    if me_gender == "male":
                        dispatcher.utter_message(text=f"Very well mister {me_lastname}.")
                    elif me_gender == "female":
                        dispatcher.utter_message(text=f"Very well miss {me_lastname}.")
                    else:
                        dispatcher.utter_message(text=f"Very well {me_lastname}.")
                else:
                    dispatcher.utter_message(text=f"Very well {me_lastname}.")
                
                return [SlotSet("me_lastname", me_lastname)]
        
        dispatcher.utter_message(text=f"Very funny {me_lastname}.")
        
        return []

class ActionSetUserSurName(Action):

    def name(self) -> Text:
        return "action_be_me"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
        ) -> List[EventType]:

        me_names = tracker.get_slot("me_names") or []
        me_surname = tracker.latest_message['entities'][0].get('surname')

        if me_surname:
            if me_surname in get_list_surnames():

                dispatcher.utter_message(response="utter_affirm_you_me", me=f"{me_surname.capitalize()}")
                
                return [SlotSet("me", me_surname.capitalize()), SlotSet("me_names", me_names.append(me_surname.capitalize()))]
        
        return []
