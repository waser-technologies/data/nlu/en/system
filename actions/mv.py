import os
import pathlib
import shutil

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .path import get_path_from_query, does_exist, parent_dir, pronounce_path
from .ls import get_list_files

def move_file(src, dst):
    shutil.move(src, dst)
    return dst

class ActionMoveFiles(Action): #mv
    
    def name(self) -> Text:
        return "action_move_which_files"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        cmd = "mv"
        as_path = tracker.latest_message['text'].split()[1:] if tracker.latest_message['text'].split()[0] == cmd else tracker.get_latest_entity_values("as_path") or get_path_from_query(tracker.latest_message['text']) or [tracker.get_slot("as_path")] or []
        
        if len(as_path) < 2:
            print("No files to move provided.")
            dispatcher.utter_message(response="utter_move_files_notprovided")
        else:
            target_path = as_path[-1]
            source_files = as_path[:-1]
            #print(f"{target_path=}")
            #print(f"{source_files=}")
            if does_exist(parent_dir(target_path)):
                dispatcher.utter_message(response="utter_move_which_files", as_path=pronounce_path(target_path))
                for p in source_files:
                    if does_exist(p):
                        #print(f"runing: mv {p=} {target_path=}")
                        move_file(p, target_path)
                    else:
                        #print("Move source does not exist.")
                        dispatcher.utter_message(response="utter_move_source_notexist", as_path=pronounce_path(p))
            else:
                #print("Move target does not exist.")
                dispatcher.utter_message(response="utter_move_target_notexist", as_path=pronounce_path(parent_dir(target_path)))

        return []