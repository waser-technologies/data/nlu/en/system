import os

from pathlib import Path

from .us import get_us_from_memory

MEMORY_PATH = Path("~/.assistant/memory").expanduser().absolute().as_posix()

def get_me_from_memory():
    _, me = get_us_from_memory(f'{MEMORY_PATH}', else_me={}, else_you={})
    return me
