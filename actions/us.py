import pickle
import os

from pathlib import Path

ME = os.environ.get('USERNAME', 'user').lower()

def set_us_in_memory(me, you, memory):
    
    # Set me up...
    me_path = f"{memory}/{ME}"
    Path(me_path).parent.mkdir(exist_ok=True)
    with open(me_path, 'wb') as me_file:
        pickle.dump(me, me_file)
    
    # ...To set you up
    you_path = f"{memory}/you_{ME}"
    you['me'] = ME
    Path(you_path).parent.mkdir(exist_ok=True)
    with open(you_path, 'wb') as you_file:
        pickle.dump(you, you_file)
    
    return you, me

def update_us(me, you, memory):
    return set_us_in_memory(me, you, memory)

def get_us_from_memory(memory, else_me={}, else_you={}):
    me_path = Path(f"{memory}/{ME}")
    you_path = Path(f"{memory}/you_{ME}")
    if me_path.exists() and me_path.is_file():
        with open(me_path.as_posix(), 'rb') as me_file:
            me = pickle.load(me_file)
            if you_path.exists() and you_path.is_file():
                with open(you_path, 'rb') as you_file:
                    you = pickle.load(you_file)
                    return you, me
            else:
                return update_us(me, you, memory)
    else:
        return update_us(else_me, else_you, memory)
    return else_you, else_me