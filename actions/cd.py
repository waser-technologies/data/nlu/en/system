import os
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .environ import get_env, set_env
from .pwd import get_pwd, set_pwd
from .path import (
    get_path_from_query, 
    does_not_exist, 
    does_exist, 
    is_dir, 
    pronounce_path
)

def change_dir(dir):
    return set_pwd(dir)

class ActionChangeCurrentDirectory(Action): #cd
    
    def name(self) -> Text:
        return "action_define_dir_it_currently"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        home_path = get_env('HOME')
        as_path = get_path_from_query(tracker.latest_message['text']) or [home_path]
        
        for p in as_path:
            if does_not_exist(p):
                dispatcher.utter_message(response="utter_be_none_dir_specified", pwd=pronounce_path(p))
                # Would you like to create such directory?
            elif does_exist(p) and is_dir(p):
                pwd = change_dir(p)
                dispatcher.utter_message(response="utter_be_which_dir_currently", pwd=pronounce_path(pwd))

        return []
