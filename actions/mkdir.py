import os
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .pwd import get_pwd
from .path import get_path_from_query, make_dir, pronounce_path

class ActionMakeDirectory(Action): #mkdir
    
    def name(self) -> Text:
        return "action_make_which_dirs"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        # filepath exists -> [paths,]
        # cmd [paths] -> [paths,]
        # create {as_path} -> [as_path]
        # else -> []
        cmd = "mkdir"
        as_path = get_path_from_query(tracker.latest_message['text']) or tracker.latest_message['text'].split()[1:] if tracker.latest_message['text'].split()[0] == cmd else tracker.get_latest_entity_values("as_path") or [tracker.get_slot("as_path")] or []

        if not as_path:
            dispatcher.utter_message(response="utter_make_none_dir")

        #print(f"Listing files for {as_path}")
        for p in as_path:
            make_dir(p)
            dispatcher.utter_message(response="utter_make_which_dirs", as_path=pronounce_path(p))

        return []
