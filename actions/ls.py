import os
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .pwd import get_pwd
from .path import (
    get_path_from_query, 
    does_exist, 
    does_not_exist, 
    is_dir, 
    is_file, 
    pronounce_path
)

def get_list_files(path=None):
    return os.listdir(path)

class ActionGetList(Action): #ls
    
    def name(self) -> Text:
        return "action_list_which_files"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        as_path = get_path_from_query(tracker.latest_message['text']) or [get_pwd()]

        #print(f"Listing files for {as_path}")
        for p in as_path:
            if does_not_exist(p):
                #print(f"{as_path} does not exist.")
                dispatcher.utter_message(response="utter_list_dir_notexist", as_path=pronounce_path(p))
            elif does_exist(p):
                dispatcher.utter_message(response="utter_list_which_files", as_path=pronounce_path(p))
                if is_dir(p):
                    for f in get_list_files(p):
                        dispatcher.utter_message(text = f"- {pronounce_path(f)}")
                elif is_file(p):
                    dispatcher.utter_message(text = f"- {pronounce_path(p)}")

        return []