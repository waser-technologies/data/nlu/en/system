import random
from rasa_sdk import Action
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional
from rasa_sdk.executor import CollectingDispatcher

from .us import ME, get_us_from_memory
from .me import MEMORY_PATH

class ActionBeWhoYou(Action):

    def name(self) -> Text:
        intent_name = "be_who_you"
        return f"action_{intent_name}"

    async def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        next_states = []

        you = tracker.get_slot("you")

        _you, _ = get_us_from_memory(f"{MEMORY_PATH}")
        _you_name = _you.get('agent', None)

        if not _you_name:
            next_states.append([FollowupAction("action_introduce_first_contact")])
        elif not you and _you_name:
            next_states.append([SlotSet('you', _you_name.capitalize())])

        agent_name = _you_name.capitalize() or you.capitalize() or "Assistant"

        dispatcher.utter_message(response="utter_be_who_you", you=agent_name)
        
        return next_states

class ActionBeWhoMe(Action):

    def name(self) -> Text:
        intent_name = "be_who_me"
        return f"action_{intent_name}"

    async def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        next_states = []

        me = tracker.get_slot("me")
        me_names = tracker.get_slot("me_names") or [ME.capitalize()]

        _, _me = get_us_from_memory(f"{MEMORY_PATH}")
        _me_names = _me.get('names', me_names)
        _me_surname = _me.get('surname', random.choice(_me_names))

        if not _me_surname:
            next_states.append([FollowupAction("action_introduce_first_contact")])
        elif not me and _me_surname:
            next_states.append([SlotSet('me', _me_surname.capitalize())])

        dispatcher.utter_message(response="utter_be_who_me", me=user_surname)
        
        return next_states
