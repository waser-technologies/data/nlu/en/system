import os
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .pwd import get_pwd
from .path import get_path_from_query, is_file, does_not_exist, pronounce_path

def get_read_file(file_path):
    if is_file(file_path):
        with open(file_path, 'r') as f:
            return f.read()

class ActionReadFile(Action): #cat
    
    def name(self) -> Text:
        return "action_read_which_files"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        as_path = get_path_from_query(tracker.latest_message['text']) or []

        #print(f"Listing files for {as_path}")
        for p in as_path:
            dispatcher.utter_message(response="utter_read_which_files", as_path=pronounce_path(p))
            if is_file(p):
                r = get_read_file(p)
                if not r:
                    dispatcher.utter_message(response="utter_read_file_empty", as_path=pronounce_path(p))
                else:
                    dispatcher.utter_message(text=r)
            elif does_not_exist(p):
                dispatcher.utter_message(response="utter_read_file_notexist", as_path=pronounce_path(p))

        return []