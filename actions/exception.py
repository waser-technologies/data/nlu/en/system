
class FileNotExistsOrNotFileError(Exception):
    def __init__(self, does_exist, is_file, message=None):
        self.does_exist = does_exist
        self.is_file = is_file
        self.message = message or f"{does_exist=} but {is_file=}; both must be positive. Download https://raw.githubusercontent.com/RasaHQ/rasa-2.x-form-examples/main/06-custom-name-experience/data/names.txt and put it as ~/.assistant/data/en/nlu/SmallTalk/lookup/names.txt"
        super().__init__(self.message)