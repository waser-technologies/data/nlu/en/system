import os

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

def echo(text):
    text = text.replace("\"", "")
    text = text.replace("\'", "")
    return text

class ActionEchoMessage(Action): #echo/say
    
    def name(self) -> Text:
        return "action_echo"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        cmds = ["echo", "say"]
        as_message = " ".join(tracker.latest_message['text'].split()[1:]) if tracker.latest_message['text'].split()[0].lower() in cmds else tracker.get_slot("as_message") or "\n"

        m = echo(as_message)
        dispatcher.utter_message(text=m)

        return []