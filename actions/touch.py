import os
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .pwd import get_pwd
from .path import get_path_from_query, touch, pronounce_path

class ActionTouchFile(Action): #touch
    
    def name(self) -> Text:
        return "action_touch_which_files"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        cmd = "touch"
        as_path = tracker.latest_message['text'].split()[1:] if tracker.latest_message['text'].split()[0] == cmd else get_path_from_query(tracker.latest_message['text']) or [get_pwd()]

        #print(f"Listing files for {as_path}")
        for p in as_path:
            touch(p)
            dispatcher.utter_message(response="utter_touch_which_files", as_path=pronounce_path(p))

        return []