import os
import shutil

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

from .pwd import get_pwd
from .path import get_path_from_query, is_dir, is_file, does_exist, pronounce_path

def remove_file(file_path):
    if is_dir(file_path):
        shutil.rmtree(file_path)
    elif is_file(file_path):
        os.remove(file_path)
    return

class ActionRemoveFiles(Action): #rm
    
    def name(self) -> Text:
        return "action_remove_which_files"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        cmd = "rm"
        as_path = tracker.latest_message['text'].split()[1:] if tracker.latest_message['text'].split()[0] == cmd else get_path_from_query(tracker.latest_message['text']) or []

        if not as_path:
            dispatcher.utter_message(response="utter_remove_files_notprovided")
        else:
            #print(f"Removing files {as_path}")
            for p in as_path:
                remove_file(p)
                dispatcher.utter_message(response="utter_remove_which_files", as_path=pronounce_path(p))

        return []