import os
import random

from pathlib import Path

def choose(from_list):
    return random.choice(from_list)

def get_doc_name(ext):
    m = {
        '.txt': ["text document", "text file"],
        '.wav': ["wave audiofile", "audio file"],
        '.png': ["image document", "image file"],
        '.docx': ["word document"],
        '.odt': ["office document"],
        '.pdf': ["pdf document"],
        '.mov': ["apple quicktime movie file"],
        'mp3': ["mp3 audio file"],
        # add more here
    }
    return m.get(ext, ["file", "document"])

def pronounce_path(path_to_pronounce):
    # /home/waser/Documents/test.txt -> \
    # (document|file) "test.txt" of folder "Desktop" of folder "waser" of folder "home" of startup disk
    dir_name = ["directory", "folder"]
    root_name = ["startup disk", "root directory"]
    home_name = ["home", "home directory", "your personal directory"]
    the_name = ["the", ""]
    of_name = ["of"]
    at_name = ["at"]
    inside_name = ["inside of", "in", "under"]

    at_home = ["at home", "inside your home directory", "home sweet home"]

    _f = os.path.basename(path_to_pronounce)
    f, e = os.path.splitext(_f)
    
    is_home = False
    from_home = False
    from_root = False

    if Path(path_to_pronounce).expanduser().absolute() == Path.home():
        is_home = True
    elif path_to_pronounce.startswith("/home"):
        from_root = False
        from_home = True
    elif path_to_pronounce.startswith("/"):
        from_root = True
    else:
        from_root = False
    
    if from_home:
        c = 3
    elif from_root:
        c = 1
    else:
        c = 0

    if not is_home:
        p = Path(path_to_pronounce).parent.as_posix().split(os.path.sep)[c:]
        p.reverse()

        pronounce = f" {choose(inside_name)} {choose(the_name)} {choose(dir_name)} ".join(p)

    if is_home:
        pronounce = choose(at_home)
    elif from_home:
        if e:
            doc_name = get_doc_name(e)
            pronounce = choose(doc_name) + f" {f} {choose(inside_name)} {choose(dir_name)} " + pronounce + f" {choose(inside_name)} {choose(home_name)}"
        else:
            pronounce = choose(dir_name) + f" {f} {choose(inside_name)} {choose(dir_name)} " + pronounce + f" {choose(inside_name)} {choose(home_name)}"
    elif from_root:
        if e:
            doc_name = get_doc_name(e)
            pronounce = choose(doc_name) + f" {f} {choose(inside_name)} {choose(dir_name)} " + pronounce + f" {choose(inside_name)} {choose(root_name)}"
        else:
            pronounce = choose(dir_name) + f" {f} {choose(inside_name)} {choose(dir_name)} " + pronounce + f" {choose(inside_name)} {choose(root_name)}"
    else:
        if e:
            doc_name = get_doc_name(e)
            pronounce = choose(doc_name) + f" {f} {choose(inside_name)} {choose(dir_name)} " + pronounce
        else:
            pronounce = choose(dir_name) + f" {f} {choose(inside_name)} {choose(dir_name)} " + pronounce    
    return pronounce

def get_path_from_query(query):
    q = query.split()
    pl = []
    for p in q:
        if os.path.exists(p):
            pl.append(p)
        elif Path(os.path.expanduser(p)).exists():
            pl.append(os.path.expanduser(p))
        elif Path(os.path.expandvars(p)).exists():
            pl.append(os.path.expandvars(p))
        elif Path(os.path.abspath(p)).exists():
            pl.append(os.path.abspath(p))

    return pl

def touch(file_path):
    p = Path(file_path)
    if not p.parent.exists():
        p.parent.mkdir(parents=True, exist_ok=True)
    p.touch()
    return p

def make_dir(file_path):
    d = Path(file_path)
    d.mkdir(parents=True, exist_ok=True)
    return d

def parent_dir(file_path):
    return Path(file_path).absolute().parent

def does_exist(file_path):
    return os.path.exists(file_path)

def does_not_exist(file_path):
    return not does_exist(file_path)

def is_file(file_path):
    return os.path.isfile(file_path)

def is_not_file(file_path):
    return not is_file(file_path)

def is_dir(file_path):
    return os.path.isdir(file_path)

def is_not_dir(file_path):
    return not is_dir(file_path)